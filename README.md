# dominion-translations
A Dominion kártyajáték kiegészítőinek fordítása magyar nyelvre.

Jelenlegi fordítások:

Kiegészítő      | Státusz | Teendők                 |
:-              | :-      | :-                      |
Dominion 2nd ed | 90%     | Tisztítás               |
Intrigue 2nd ed | 90%     | Tisztítás+szétválasztás |
Seaside         | 80%     | Tisztítás, ellenőrzés   |
Alchemy         | 100%    | -                       |
Prosperity      | 80%     | Tisztítás, ellenőrzés   |
Cornucopia      | 80%     | Tisztítás, ellenőrzés   |
Hinterlands     | 80%     | Tisztítás, ellenőrzés   |
Dark Ages       | 80%     | Tisztítás, ellenőrzés   |
Guilds          | 80%     | Tisztítás, ellenőrzés   |
Adventures      | 80%     | Tisztítás, ellenőrzés   |
Empires         | 80%     | Tisztítás, ellenőrzés   |
Nocturne        | 0%      |                         |
Renaissance     | 0%      |                         |

Egyéb teendők:  
Scriptek írása egyéb fájlok létrehozására, nyomtatáshoz és kivágáshoz


# Segítés  
Szívesen fogadok segítséget fordításban, javításokban, szerkesztésben. Ezt ha nem értesz a git rendszerhez legegyszerűbben a kreest503@gmail.com email címre írva tudod megtenni.  
Ha értesz hozzá, kisebb problémákra nyiss egy új "Issue"-t, vagy nagyobb változtatásokhoz hozz létre saját branchet a projektről.  
Az alapformátum a LibreOffice tömörítetlen formátuma (*.fods), ezek a rawods mappában találhatóak, a többi fájl ezek alapján lesz legenerálva.  
A converter mappában találhatóak a különböző fájlformátumokra való konvertálás scriptjei, ezek az [unoconv](https://github.com/dagwieers/unoconv) programot használják.
