import socket  # only needed on win32-OOo3.0.0
import uno

# get the uno component context from the PyUNO runtime
localContext = uno.getComponentContext()

# create the UnoUrlResolver
resolver = localContext.ServiceManager.createInstanceWithContext(
				"com.sun.star.bridge.UnoUrlResolver", localContext )

# connect to the running office
ctx = resolver.resolve( "uno:socket,host=localhost,port=2002;urp;StarOffice.ComponentContext" )
smgr = ctx.ServiceManager

# get the central desktop object
desktop = smgr.createInstanceWithContext( "com.sun.star.frame.Desktop",ctx)

# access the current writer document
model = desktop.getCurrentComponent()

active_sheet = model.CurrentController.ActiveSheet

for r in range(0, 10000):
    if active_sheet.getCellByPosition(1, r).String == '':
        break

for i in range(0, r):
    active_sheet.getCellByPosition(2, i).String = active_sheet.getCellByPosition(2, i).String[:active_sheet.getCellByPosition(2, i).String.find('\n')]
